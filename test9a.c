#include <stdio.h>
void swap (int*,int*);
int main ()
{
    int n1,n2;
    printf ("enter two numbers");
    scanf ("%d %d",&n1,&n2);
    printf ("numbers before swapping are:\nnum1 = %d and num2 = %d\n",n1,n2);
    swap (&n1,&n2);
    printf ("numbers after swapping are:\nnum1 = %d and num2 = %d",n1,n2);
    return 0;
}
void swap (int *n1, int *n2)
{
    int t;
    t=*n1;
    *n1=*n2;
    *n2=t;
}
