#include <stdio.h>
int main ()
{
    int i,j,r,c;
    printf ("enter the no. of rows and columns of the two matrices");
    scanf ("%d %d",&r,&c);
    int a[r][c],b[r][c],sum[r][c];
    printf ("enter the elements of first matrix");
    for (i=0;i<r;i++)
    {
        for (j=0;j<c;j++)
        scanf ("%d",&a[i][j]);
    }
    printf ("enter the elements of second matrix");
    for (i=0;i<r;i++)
    {
        for (j=0;j<c;j++)
        scanf ("%d",&b[i][j]);
    }
    for (i=0;i<r;i++)
    {
        for (j=0;j<c;j++)
        sum[i][j]=a[i][j]+b[i][j];
    }
    printf ("the resultant matrix is :\n");
    for (i=0;i<r;i++)
    {
        for (j=0;j<c;j++)
        printf ("%d ",sum[i][j]);
        printf ("\n");
    }
    return 0;
}