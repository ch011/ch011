#include <stdio.h>
int main ()
{
    struct emp_name
    {
        char first_name[20], middle_name[20], last_name[20];
    };
    struct emp_joining_date
    {
        int d, month, year;
    };
    struct emp_details
    {
        int emp_id, emp_salary;
        struct emp_name name;
        struct emp_joining_date date;
    }emp;
    printf ("enter employee's name");
    scanf ("%s %s %s",emp.name.first_name,emp.name.middle_name,emp.name.last_name);
    printf ("enter employee's id and salary");
    scanf ("%d %d", &emp.emp_id,&emp.emp_salary); 
    printf ("enter employee's joining date");
    scanf ("%d %d %d", &emp.date.d,&emp.date.month,&emp.date.year);
    printf ("The details of the employee are as follows:\n");
    printf ("Name : %s %s %s\n",emp.name.first_name,emp.name.middle_name,emp.name.last_name);
    printf ("ID : %d\n",emp.emp_id);
    printf ("Salary : %d\n",emp.emp_salary);
    printf ("Joining Date : %d/%d/%d\n",emp.date.d,emp.date.month,emp.date.year);
    return 0;
}