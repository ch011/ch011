#include <stdio.h>
int main ()
{
    struct student_details
    {
        char name[50],section[5];
        int class,r_no,marks; 
    }stud1,stud2;
    printf ("enter name,class,section,roll no. and marks of first student\n");
    scanf ("%s %d %s %d %d",stud1.name,&stud1.class,stud1.section,&stud1.r_no,&stud1.marks);
    printf ("enter name,class,section,roll no. and marks of second student\n");
    scanf ("%s %d %s %d %d",stud2.name,&stud2.class,stud2.section,&stud2.r_no,&stud2.marks);
    printf ("The details of first student are :\n");
    printf ("Name : %s\n",stud1.name);
    printf ("Class : %d\n",stud1.class);
    printf ("Section : %s\n",stud1.section);
    printf ("Roll No. : %d\n",stud1.r_no);
    printf ("Marks : %d\n",stud1.marks);
    printf ("The details of second student are :\n");
    printf ("Name : %s\n",stud2.name);
    printf ("Class : %d\n",stud2.class);
    printf ("Section : %s\n",stud2.section);
    printf ("Roll No. : %d\n",stud2.r_no);
    printf ("Marks : %d\n",stud2.marks);
    if (stud1.marks>stud2.marks)
    printf ("%s scored the highest marks",stud1.name);
    else
    printf ("%s scored the highest marks",stud2.name);
    return 0;
}